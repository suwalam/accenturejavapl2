package pl.sda.collections;

import java.util.LinkedList;
import java.util.List;

public class LinkedListMain {

    public static void main(String[] args) {

        LinkedList<Integer> linkedList = new LinkedList<>();

        linkedList.add(12);
        linkedList.add(43);
        linkedList.add(55);

        System.out.println(linkedList.poll()); //zwraca i usuwa głowę (pierwszy element)
        System.out.println(linkedList.pop()); //zwraca i usuwa głowę (pierwszy element)
        System.out.println(linkedList.getFirst()); //zwraca głowę (pierwszy element)
        System.out.println(linkedList.element()); //zwraca głowę (pierwszy element)
        System.out.println(linkedList.getLast()); //zwraca ogon (ostatni element)
        System.out.println(linkedList.peekLast()); //zwraca ogon (ostatni element)

        for (Integer i : linkedList) {
            System.out.print(i + " ");
        }

    }

}
