package pl.sda.collections;

import java.util.HashMap;
import java.util.Map;

public class HashMapMain {

    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "poniedziałek");
        map.put(2, "wtorek");
        map.put(3, "środa");
        map.put(4, "czwartek");
        map.put(5, "piątek");
        map.put(6, "sobota");
        map.put(7, "niedziela");
        map.put(null, "null string");

        System.out.println(map.get(1));
        System.out.println(map.containsKey(7)); //true
        System.out.println(map.remove(7));
        System.out.println(map.containsKey(7)); //false

        System.out.println(map.containsKey(null)); //true

        for (Integer i : map.keySet()) {
            System.out.print(i + " ");
        }

        System.out.println();

        for (String s : map.values()) {
            System.out.print(s + " ");
        }

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }

    }

}
