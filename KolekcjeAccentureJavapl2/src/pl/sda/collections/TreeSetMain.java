package pl.sda.collections;

import com.sun.source.tree.Tree;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class TreeSetMain {

    public static void main(String[] args) {

        TreeSet<Integer> treeSet = new TreeSet<>();

        treeSet.add(12);
        treeSet.add(54);
        treeSet.add(76);
        treeSet.add(59);
        treeSet.add(32);

        for (Integer i : treeSet) {
            System.out.print(i + " ");
        }
        //**************************************

        System.out.println();//nowa linia

        SortedSet<Integer> headSet = treeSet.headSet(59, true);
        for (Integer i : headSet) {
            System.out.print(i + " ");
        }

        headSet.add(10);

        System.out.println();//nowa linia

        for (Integer i : treeSet) {
            System.out.print(i + " ");
        }

        //**************************************
        System.out.println();
        SortedSet<Integer> tailSet = treeSet.tailSet(32);

        for (Integer i : tailSet) {
            System.out.print(i + " ");
        }

       //**********************************************
        SortedSet<Integer> subSet = treeSet.subSet(32, 59);
        System.out.println();

        for (Integer i : subSet) {
            System.out.print(i + " ");
        }

        System.out.println();

        System.out.println(treeSet.first());//min
        System.out.println(treeSet.last());//max

    }

}
