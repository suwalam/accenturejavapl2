package pl.sda.collections;

import java.util.HashSet;

public class HashSetMain {

    public static void main(String[] args) {

        HashSet<Integer> hashSet = new HashSet<>();

        hashSet.add(10);
        hashSet.add(34);
        hashSet.add(11);
        boolean added = hashSet.add(10);

        System.out.println(added);
        System.out.println(hashSet.size()); //3

        System.out.println(hashSet.contains(34)); //true

        for (Integer i : hashSet) {
            System.out.print(i + " ");
        }
    }

}
