package pl.sda.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayListMain {

    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<>();
        System.out.println(arrayList.isEmpty()); //true

        arrayList.add(10);
        arrayList.add(40);
        arrayList.add(54);
        arrayList.add(323);
        arrayList.add(10);

        System.out.println(arrayList.size()); //5
        System.out.println(arrayList.contains(54));//true

        System.out.println(arrayList.get(1));
        System.out.println(arrayList.get(arrayList.size() -1));

        arrayList.remove(Integer.valueOf(10));

        for (Integer i : arrayList) {
            System.out.print(i + " ");
        }

        System.out.println(); //nowa linia

        System.out.println(arrayList.indexOf(54)); //1

//        Collections.sort(arrayList); //sortowanie w porządku naturalnym

        Collections.sort(arrayList, Comparator.reverseOrder()); //sortowanie w porządku odwrotnym do naturalnego

        for (Integer i : arrayList) {
            System.out.print(i + " ");
        }
    }

}
