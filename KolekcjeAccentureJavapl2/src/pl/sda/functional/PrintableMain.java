package pl.sda.functional;

public class PrintableMain {

    public static void main(String[] args) {

        Printable printable = o -> System.out.println("Print from lambda: " + o);

//        Printable printable = new Printable() {
//            @Override
//            public void print(Object obj) {
//                System.out.println("Print from anonymous: " + obj);
//            }
//        };

        printable.print("example text");

    }

}
