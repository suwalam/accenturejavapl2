package pl.sda.functional;

import java.math.BigDecimal;

public class ComputableMain {

    public static void main(String[] args) {

        Computable adder = (a, b) -> a + b;

        Computable multiplier = (a, b) -> a * b;

        int [] array = {1, 2, 5};
        System.out.println(adder.count(array));

        System.out.println(adder.compute(10.5, 44.3));
        System.out.println(multiplier.compute(10.0, 44.3));

        Computable power = (b, p) -> {

            double result = 1;

            if (p == 0) {
                return result;
            }

            if (p < 0) {
                throw new IllegalArgumentException("Power is invalid");
            }

            if (b == 0) {
                return b;
            }

            for (int i = 0; i < p; i++) {
                result = result * b;
            }

            return result;
        };
        System.out.println(power.compute(2, 10));


    }

}
