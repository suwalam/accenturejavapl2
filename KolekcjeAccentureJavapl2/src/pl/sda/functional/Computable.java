package pl.sda.functional;

@FunctionalInterface
public interface Computable {

    double compute(double a, double b);

    default int count(int [] array) {
        if (array == null || array.length == 0) {
            return 0;
        }

        return array.length;
    }

}
