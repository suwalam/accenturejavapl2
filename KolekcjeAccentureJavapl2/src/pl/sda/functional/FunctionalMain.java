package pl.sda.functional;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalMain {

    public static void main(String[] args) {

        Predicate<String> isStringLengthIsGreaterThan5 = (s) -> s.length() > 5;
        System.out.println(isStringLengthIsGreaterThan5.test("Ala"));

        //*************************************************
        Function<String, Integer> countA = (s) -> {
            int sum = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == 'A' || s.charAt(i) == 'a') {
                    sum++;
                }
            }
            return sum;
        };
        System.out.println(countA.apply("Ala ma kota a kot ma Alę"));
        //*************************************************

        Consumer<String> consumer = (s) -> System.out.println("String from argument: " + s);
        consumer.accept("Ala ma kota");

        //*************************************************
        Supplier<Integer> supplier = () -> 10;
        System.out.println(supplier.get());
        System.out.println(supplier.get());
        System.out.println(supplier.get());

    }

}
