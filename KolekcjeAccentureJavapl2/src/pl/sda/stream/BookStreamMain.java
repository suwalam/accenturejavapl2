package pl.sda.stream;

import pl.sda.compare.Book;
import pl.sda.compare.BookComparator;
import pl.sda.compare.Library;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookStreamMain {

    public static void main(String[] args) {

        Book b1 = new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 12345, LocalDate.now().minusYears(100));
        Book b2 = new Book("Pan Tadeusz", "Adam Mickiewicz", 54321, LocalDate.now().minusYears(120));
        Book b3 = new Book("Balladyna", "Juliusz Słowacki", 54311, LocalDate.now().minusYears(130));
        Book b4 = new Book("W Pustyni i w Puszczy", "Henryk Sienkiewicz", 433222, LocalDate.now().minusYears(140));
        Book b5 = new Book("Dziady", "Adam Mickiewicz", 5434321, LocalDate.now().minusYears(114));
        Book b6 = new Book("Lokomotywa", "Jan Brzechwa", 54341111, LocalDate.now().minusYears(143));

        List<Book> bookList = Arrays.asList(b1, b2, b3, b4, b5, b6);

        Library lib1 = new Library("Warszawa", Arrays.asList(b1, b2, b3));
        Library lib2 = new Library("Kraków", Arrays.asList(b4, b5, b6));
        List<Library> libraries = Arrays.asList(lib1, lib2);

        //printBooksForGivenAuthor(bookList, "Adam Mickiewicz");

        //printBookTitlesAndAuthorsSortedByReleaseDate(bookList);

        int reduced = Stream.of(1, 3, 5, 7, 8)
                .reduce((a, b) -> a + b)
                .get();
        System.out.println(reduced);

//        printCountOfBooksWhereReleaseDateIsAfterGivenYear(bookList, 1900);

        //printBooksFromLibraries(libraries);

        boolean anyMatch = bookList.stream()
                .anyMatch(b -> b.getReleaseDate().isAfter(LocalDate.of(1900, 1, 1)));

        System.out.println(anyMatch);

        boolean allMatch = bookList.stream()
                .allMatch(b -> b.getTitle().length() > 3);
        System.out.println(allMatch);


//        optionalUsage(bookList);
        optionalUsage(new ArrayList<>());


    }

    private static void printBooksForGivenAuthor(List<Book> bookList, String author) {
        bookList.stream()
                .filter(b -> b.getAuthor().equals(author))
                .sorted(Comparator.comparingInt(Book::getIsbn))
                .map(Book::getTitle)
                .forEach(System.out::println);
    }

    //wypisz tytuł - autor posortowane po dacie wydania
    private static void printBookTitlesAndAuthorsSortedByReleaseDate(List<Book> bookList) {
        bookList.stream()
                .sorted(Comparator.comparing(Book::getReleaseDate))
                .map(b -> b.getTitle() + " - " + b.getAuthor())
                .forEach(System.out::println);
    }

    private static void printCountOfBooksWhereReleaseDateIsAfterGivenYear(List<Book> bookList, int year) {
        long count = bookList.stream()
                .filter(b -> b.getReleaseDate().getYear() > year)
                .count();

        System.out.println("There is " + count + " books released after " + year + " year");
    }

    private static void printBooksFromLibraries(List<Library> libraries) {
        libraries.stream()
                .filter(lib -> lib.getCity().equals("Warszawa"))
                .map(lib -> lib.getBooks())
                .flatMap(list -> list.stream())
                .forEach(System.out::println);

        //kod równoważny do powyższego strumienia
        /*for (Library lib : libraries) {
            if (lib.getCity().equals("Warszawa")) {
                for (Book book : lib.getBooks()) {
                    System.out.println(book);
                }
            }
        }*/
    }

    private static void optionalUsage(List<Book> bookList) {

        Optional<Book> optional = bookList.stream().findFirst();

        if (optional.isPresent()) {
            System.out.println(optional.get().getTitle());
        }

        optional.ifPresent(System.out::println);

        Book defaultBook = optional.orElse(new Book("Domyśna", "Domyślny", 0, LocalDate.now()));
        System.out.println("Domyślna książka: " + defaultBook);

        Book orElseGetBook = optional.orElseGet(() -> new Book("Domyśna", "Domyślny", 0, LocalDate.now()));
        System.out.println("orElseGetBook książka: " + orElseGetBook);

        optional.ifPresentOrElse(System.out::println, () -> System.out.println("Obiekt książka nie istnieje"));

        optional.orElseThrow(() -> new IllegalStateException("Pusty strumień"));

    }

}
