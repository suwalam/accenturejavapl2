package pl.sda.strategy;

public class UpperTextFormatter implements TextFormatterStrategy {

    @Override
    public String format(String text) {
        return text.toUpperCase();
    }
}
