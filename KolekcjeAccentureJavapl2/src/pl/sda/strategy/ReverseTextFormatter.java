package pl.sda.strategy;

public class ReverseTextFormatter implements TextFormatterStrategy {

    @Override
    public String format(String text) {
        StringBuilder sb = new StringBuilder(text);
        return sb.reverse().toString();
    }
}
