package pl.sda.strategy;

public class StrategyMain {

    public static void main(String[] args) {

        TextEditor textEditor = new TextEditor(new UpperTextFormatter());
        textEditor.viewTextFormatted("Ala ma kota");

        textEditor.setStrategy(new ReverseTextFormatter());
        textEditor.viewTextFormatted("Ala ma kota");

    }

}
