package pl.sda.strategy;

public class TextEditor {

    private TextFormatterStrategy strategy;

    public TextEditor(TextFormatterStrategy strategy) {
        this.strategy = strategy;
    }

    public void viewTextFormatted(String text) {
        System.out.println(strategy.format(text));
    }

    public void setStrategy(TextFormatterStrategy strategy) {
        this.strategy = strategy;
    }
}
