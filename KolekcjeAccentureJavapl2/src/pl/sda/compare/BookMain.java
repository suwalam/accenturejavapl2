package pl.sda.compare;

import java.time.LocalDate;

public class BookMain {

    public static void main(String[] args) {

        Book x = new Book("Balldyna", "Juliusz Słowacki", 12345, LocalDate.now().minusYears(120));
        Book y = new Book("Balldyna", "Juliusz Słowacki", 12345, LocalDate.of(1865, 4, 21));
        Book z = new Book("Balldyna", "Juliusz Słowacki", 12345, LocalDate.now().minusYears(141));

        System.out.println(x.equals(x)); //true
        //symetryczność
        System.out.println(x.equals(y)); //true
        System.out.println(y.equals(x)); //true

        System.out.println(x.equals(null)); //false
        System.out.println(x.equals(54)); //false

        //przechodniość
        System.out.println(x.equals(y));//true
        System.out.println(x.equals(z)); //true
        System.out.println(y.equals(z)); //true

        System.out.println(x.hashCode());
        System.out.println(y.hashCode());
        System.out.println(z.hashCode());

        Book a = new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 54321, LocalDate.now().minusYears(132));
        System.out.println(a.equals(x)); //false
        System.out.println(a.hashCode());

        System.out.println(Character.valueOf('A').hashCode()); //ASCII table
        System.out.println(Character.valueOf('a').hashCode()); //ASCII table
        System.out.println(Character.valueOf(' ').hashCode()); //ASCII table

        byte b = (byte) 200; //-128 - 127
        System.out.println(b);

        System.out.println(a.compareTo(x)); //liczba dodatnia - a jest większe od x -
        // Balladyna występuje wcześniej w alfabecie dlatrego jest mniejsza

        System.out.println(x.compareTo(a)); //liczba ujemna

        System.out.println(x.compareTo(y)); // liczba 0 ponieważ to są te same tytuły

        BookComparator bookComparator = new BookComparator();
        System.out.println(bookComparator.compare(x, a)); //liczba ujemna

    }

}
