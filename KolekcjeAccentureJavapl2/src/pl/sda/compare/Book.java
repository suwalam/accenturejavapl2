package pl.sda.compare;

import java.time.LocalDate;
import java.util.Objects;

public class Book implements Comparable<Book> {

    private final String title;

    private final String author;

    private final int isbn;

    private final LocalDate releaseDate;

    public Book(String title, String author, int isbn, LocalDate releaseDate) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getIsbn() {
        return isbn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return isbn == book.isbn && title.equals(book.title) && author.equals(book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, isbn);
    }

    @Override
    public int compareTo(Book o) {
        return title.compareTo(o.getTitle()); //A - Z
//        return o.getTitle().compareTo(title); //Z - A
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isbn=" + isbn +
                ", releaseDate=" + releaseDate +
                '}';
    }
}
