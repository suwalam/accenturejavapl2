package pl.sda.factorymethod;

public class EmployeeFactory {

    public static Employee createEmployee(String name, String surname, EmployeeType employeeType) {

        switch (employeeType) {

            case ARCHITECT:
                return new Architect(name, surname);
            case PROGRAMMER:
                return new Programmer(name, surname);
            case TESTER:
                throw new UnsupportedOperationException("Nieobsługiwana operacja");

            default: throw new IllegalArgumentException("Unexpected type: " + employeeType);
        }
    }

}
