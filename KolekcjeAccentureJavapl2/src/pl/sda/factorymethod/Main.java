package pl.sda.factorymethod;

public class Main {

    public static void main(String[] args) {

        Employee architect = EmployeeFactory.createEmployee("Jan", "Kowalski", EmployeeType.ARCHITECT);

        Employee programmer = EmployeeFactory.createEmployee("Marek", "Nowak", EmployeeType.PROGRAMMER);

        System.out.println(architect.calculateSalary());
        System.out.println(programmer.calculateSalary());
    }

}
