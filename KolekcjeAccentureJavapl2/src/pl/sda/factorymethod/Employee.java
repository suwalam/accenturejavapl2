package pl.sda.factorymethod;

public abstract class Employee {

    private String name;

    private String surname;

    private EmployeeType employeeType;

    public Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    protected void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    public abstract double calculateSalary();
}
