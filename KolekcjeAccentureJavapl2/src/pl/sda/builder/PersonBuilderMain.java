package pl.sda.builder;

public class PersonBuilderMain {

    public static void main(String[] args) {

        Person.Builder builder = new Person.Builder();

        Person person = builder
                .withName("Jan")
                .withSurname("Kowalski")
                .withPesel("12021278909")
                .build();

        System.out.println(person);

        //lombok
        //@Builder
        //@Data

    }

}
