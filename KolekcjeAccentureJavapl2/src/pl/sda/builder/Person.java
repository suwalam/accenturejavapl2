package pl.sda.builder;

public class Person {

    private String name;

    private String surname;

    private String pesel;

    private String address;

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public static class Builder {

        private Person person = new Person();

        public Builder withName(String name) {
            person.name = name;
            return this;
        }

        public Builder withSurname(String surname) {
            person.surname = surname;
            return this;
        }

        public Builder withPesel(String pesel) {
            person.pesel = pesel;
            return this;
        }

        public Builder withAddress(String address) {
            person.address = address;
            return this;
        }

        public Person build() {
            Person copy = person;
            person = new Person();
            return copy;
        }

    }
}
