package pl.sda.order.service;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceTest {

    private OrderService orderService;

    @BeforeEach
    public void setUp() {
        System.out.println("Before each test");
        orderService = new OrderService();
    }

    @Test
    public void shouldCreateSecondOrder() {
        //given

        //pierwsze wywołanie - przygotowanie obiektu do testu
        Integer firstOrderId = orderService.createOrder();
        Integer expectedSecondOrderId = firstOrderId + 1;
        //when
        Integer secondOrderId = orderService.createOrder();

        //then
        assertNotNull(secondOrderId);
        assertEquals(expectedSecondOrderId, secondOrderId);
    }

    @Test
    public void shouldRemoveProductFromOrder() {
        //given
        Integer orderId = orderService.createOrder();
        Integer productId = orderService.addProductToOrder(orderId, "BOOK", "czasopismo", "producent", "opis");

        //when
        boolean removeResult = orderService.removeProductFromOrder(orderId, productId);

        //then
        assertTrue(removeResult);
    }

    @Test
    public void shouldThrowExceptionWhenRemoveProductFromNonExistingOrder() {
        //given
        Integer orderId = Integer.MAX_VALUE;
        Integer productId = Integer.MAX_VALUE;

        //when //then
        Assertions.assertThatThrownBy(
                () -> orderService.removeProductFromOrder(orderId, productId))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Unrecognized id " + orderId);
    }

}