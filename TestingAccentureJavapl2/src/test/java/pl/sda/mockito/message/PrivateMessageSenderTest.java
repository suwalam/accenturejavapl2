package pl.sda.mockito.message;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PrivateMessageSenderTest {

    private static final String TEXT = "hello";

    private static final String AUTHOR_ID = "michal";

    private static final String RECIPIENT_ID = "jan";

    @Mock
    private MessageProvider messageProvider;

    @Mock
    private MessageValidator messageValidator;

    @InjectMocks
    private PrivateMessageSender privateMessageSender;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Test
    public void shouldSendPrivateMessage() {
        //given
        when(messageValidator.isMessageValid(any())).thenReturn(true);
        when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);

        //when
        privateMessageSender.sendPrivateMessage(TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        verify(messageProvider).send(messageCaptor.capture(), eq(MessageType.PRIVATE));
        verify(messageValidator).isMessageValid(messageCaptor.capture());

        verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        verifyNoMoreInteractions(messageProvider);
        verifyNoMoreInteractions(messageValidator);

        Message message = messageCaptor.getValue();
        assertEquals(TEXT, message.getValue());
        assertEquals(RECIPIENT_ID, message.getRecipent());
        assertEquals(AUTHOR_ID, message.getAuthor());
        assertNotNull(message.getId());


    }


}