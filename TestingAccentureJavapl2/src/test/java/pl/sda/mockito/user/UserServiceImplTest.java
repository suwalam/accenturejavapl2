package pl.sda.mockito.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final long IDENTIFIER = 1L;

    private static final User USER = new User(IDENTIFIER, "Jan", "Testowy");

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserValidator userValidator;

    @InjectMocks
    private UserServiceImpl userService;

    //zakomentowane ponieważ adnotacja @InjectMocks robi to samo
//    @BeforeEach
//    public void setUp() {
//        userService = new UserServiceImpl(userRepository, userValidator);
//    }

    @Test
    public void shouldGetUserById() {
        //given
        when(userRepository.findById(IDENTIFIER)).thenReturn(Optional.of(USER));

        //when
        User actualUser = userService.getUserById(IDENTIFIER);

        //then
        Assertions.assertThat(actualUser).isEqualTo(USER);
        verify(userRepository).findById(IDENTIFIER);
        verifyNoMoreInteractions(userRepository);
        verifyNoInteractions(userValidator);
    }

}