package pl.sda.parametrized;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {


    //given
    @CsvSource(value = {"  test  ;TEST", " tesT;TEST", "tESt  ;TEST", "'';''"}, delimiter = ';')
    @ParameterizedTest
    public void shouldTrimAndUpperCaseInput(String input, String expected) {
        //when
        String result = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";")
    public void shouldTrimAndUpperCaseInputWithCSVFileSource(String input, String expected) {
        //when
        String result = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, result);
    }

}