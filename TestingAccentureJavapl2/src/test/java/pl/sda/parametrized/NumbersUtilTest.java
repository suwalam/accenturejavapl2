package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumbersUtilTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 5, 7, 4321, 997})
    public void shouldReturnTrueForOddNumbers(int number) {
        assertTrue(NumbersUtil.isOdd(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {12, 54, 76, 43212, 99790})
    public void shouldReturnFalseForEvenNumbers(int number) {
        assertFalse(NumbersUtil.isOdd(number));
    }


    @ParameterizedTest
    @MethodSource(value = "provideNumberWithInfoAboutParity")
    public void shouldReturnExpectedValueForGivenNumber(int number, boolean expected) {
        assertEquals(expected, NumbersUtil.isOdd(number));
    }

    private static Stream<Arguments> provideNumberWithInfoAboutParity() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(12, false),
                Arguments.of(13, true),
                Arguments.of(18, false),
                Arguments.of(0, false)
        );
    }

}