package pl.sda.order.service;

import pl.sda.order.ProductComparator;
import pl.sda.order.model.Order;
import pl.sda.order.model.Product;
import pl.sda.order.model.ProductType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class OrderService {

    private List<Order> orders = new ArrayList<>();

    public Integer createOrder() {
        Order order = new Order();
        orders.add(order);
        return order.getId();
    }

    public void printOrder(Integer id) {
        Order order = getOrderById(id);
        System.out.println("Products quantity: " + order.size());
        printProducts(order.getProducts());
    }

    public void printOrderWithSortedProducts(Integer id) {
        Order order = getOrderById(id);
        List<Product> products = order.getProducts();
        Collections.sort(products, new ProductComparator());
        System.out.println("Products quantity: " + order.size());
        printProducts(products);
    }

    public Integer addProductToOrder(Integer orderId, String type, String name, String producer, String description) {
        Order order = getOrderById(orderId);
        Product product = new Product(ProductType.valueOf(type), name, producer, description);
        order.addProduct(product);
        return product.getId();
    }

    public boolean removeProductFromOrder(Integer orderId, Integer productId) {
        Order order = getOrderById(orderId);
        return order.removeProductById(productId);
    }

    public void updateProductDescription(Integer orderId, Integer productId, String newDescription) {
        Order order = getOrderById(orderId);
        order.updateProductDescription(productId, newDescription);
    }

    private Order getOrderById(Integer id) {
        for (Order order : orders) {
            if (order.getId().equals(id)) {
                return order;
            }
        }
        throw new IllegalArgumentException("Unrecognized id " + id);
    }

    private void printProducts(List<Product> products) {
        for (int i=0; i< products.size(); i++) {
            System.out.println((i+1) + ". - " + products.get(i));
        }
    }

}
