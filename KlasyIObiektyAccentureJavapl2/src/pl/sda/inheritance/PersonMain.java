package pl.sda.inheritance;

public class PersonMain {

    public static void main(String[] args) {

        //Zakomentowane ponieważ nie można utworzyc obiektu klasy abstrakcyjnej
//        Person person = new Person("Marek", "Nowak", "99010100099");
//        person.setName("Jan");
//        person.print();
//
//        System.out.println(person.getName());
//        System.out.println(person.getPesel());

        Person student = new Student("Anna", "Kowalska", "78020443215", "IT");
        student.print();


        Student student2 = (Student) student;

        student2.setDepartment("Finanse");
        System.out.println(student2.getDepartment());


        //***********************************
        ((Student) student).setDepartment("IT");

        //************************************

        Person employee = new Person("Anna", "Nowak", "79080500098") {
            @Override
            public void print() {
                System.out.println("Emloyee name: " + getName());
            }
        };

        employee.print();
    }

}
