package pl.sda.inheritance;

public abstract class Person implements Printable {

    protected String name;

    protected String surname;

    protected String pesel;

    public Person(String name, String surname, String pesel) {
        setName(name);
        setSurname(surname);
        setPesel(pesel);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can not be null");
        }
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        if (surname == null) {
            throw new IllegalArgumentException("Surname can not be null");
        }
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        if (pesel == null || pesel.length() != 11) {
            throw new IllegalArgumentException("Pesel is invalid");
        }
        this.pesel = pesel;
    }
}


