package pl.sda.inheritance;

public interface Printable {

    int CONST = 10;

    void print();

}
