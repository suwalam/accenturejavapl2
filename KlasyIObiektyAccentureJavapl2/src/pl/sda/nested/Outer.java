package pl.sda.nested;

public class Outer {

    private int outerInt = 5;

    public static final int X = 3;

    class Inner {

        private String innerStr;

        public Inner(String innerStr) {
            this.innerStr = innerStr;
        }

        public String getInnerStr() {
            return innerStr;
        }

        public void printOuterInt() {
            System.out.println(outerInt);
        }

        public void printOuterX() {
            System.out.println(X);
        }
    }

    static class InnerStatic {

        static int a = 1;

        void printOuterX() {
            System.out.println(X);
        }

    }

    public void methodWithLocalClass() {

        class Local {
            void printOuterFields() {
                System.out.println(outerInt);
                System.out.println(X);
            }
        }

        Local local = new Local();
        local.printOuterFields();

    }

}
