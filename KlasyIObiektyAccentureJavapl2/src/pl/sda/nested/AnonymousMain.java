package pl.sda.nested;

public class AnonymousMain {

    public static void main(String[] args) {

        AbstractPrinter printer = new AbstractPrinter() {

            @Override
            public void print(Object obj) {
                System.out.println(obj);
            }
        };

        printer.print("print from anonymous class");


    }

}
