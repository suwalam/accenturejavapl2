package pl.sda.shape;

import java.sql.SQLOutput;

public class ShapeMain {

    public static void main(String[] args) {

        AbstractShape square = new Square(10);
        AbstractShape circle = new Circle(5);

        AbstractShape [] shapes = {square, circle};

        for (AbstractShape shape : shapes) {
            System.out.println("Dla kształtu " + shape.getShapeName() + " pole wynosi: " + shape.calculateArea());
        }
    }

}
