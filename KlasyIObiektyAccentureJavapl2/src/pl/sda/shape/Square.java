package pl.sda.shape;

public class Square extends AbstractShape {

    private final double a;

    public Square(double a) {
        this.a = a;
        setShapeType(ShapeType.SQUARE);
    }

    @Override
    public double calculateArea() {
        return a * a;
    }
}
