package pl.sda.shape;

public abstract class AbstractShape implements AreaCalculable {

    private ShapeType shapeType;

    public void setShapeType(ShapeType shapeType) {
        this.shapeType = shapeType;
    }

    public String getShapeName() {
        return shapeType.getName();
    }
}
