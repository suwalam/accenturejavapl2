package pl.sda.shape;

public class Circle extends AbstractShape {

    private final double r;

    public Circle(double r) {
        this.r = r;
        setShapeType(ShapeType.CIRCLE);
    }

    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(r, 2);
    }
}
