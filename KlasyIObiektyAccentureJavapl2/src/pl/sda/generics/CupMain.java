package pl.sda.generics;

public class CupMain {

    public static void main(String[] args) {

        Tea tea = new Tea("green");
        Cup<Tea> cup = new Cup<>(tea);
        cup.drink();

    }
}
